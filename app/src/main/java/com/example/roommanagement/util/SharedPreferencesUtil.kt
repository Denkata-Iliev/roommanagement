package com.example.roommanagement.util

import android.content.Context
import android.content.SharedPreferences
import com.example.roommanagement.database.entity.Role

private const val SHARED_PREFS_NAME = "user_prefs"
private const val KEY_USERID = "user_id"
private const val KEY_ROLE = "user_role"

class SharedPreferencesUtil private constructor(context: Context) {
    private val sharedPrefs: SharedPreferences

    init {
        sharedPrefs = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
    }

    private fun saveUserId(userId: Long) {
        sharedPrefs.edit().putLong(KEY_USERID, userId).apply()
    }

    fun getUserId(): Long {
        return sharedPrefs.getLong(KEY_USERID, 0)
    }

    private fun saveUserRole(role: Role?) {
        sharedPrefs.edit().putString(KEY_ROLE, role?.name).apply()
    }

    fun getUserRole(): String? {
        return sharedPrefs.getString(KEY_ROLE, "")
    }

    fun saveUserInfo(userId: Long, role: Role?) {
        saveUserId(userId)
        saveUserRole(role)
    }

    fun logout() {
        saveUserInfo(-1, null)
    }

    companion object {
        private var INSTANCE: SharedPreferencesUtil? = null

        fun getSharedPrefs(context: Context): SharedPreferencesUtil {
            return INSTANCE ?: SharedPreferencesUtil(context)
        }
    }
}