package com.example.roommanagement.util

import android.content.Context
import android.view.LayoutInflater
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.example.roommanagement.R
import com.google.android.material.textfield.TextInputLayout


object DialogUtils {
    fun showDialogBox(
        context: Context,
        title: String,
        hint: String,
        positiveButtonText: String,
        validationLiveData: LiveData<Boolean>,
        errorMessageLiveData: LiveData<String>,
        lifecycleOwner: LifecycleOwner,
        action: (String) -> Unit
    ) {
        val inflater = LayoutInflater.from(context)
        val dialogView = inflater.inflate(R.layout.dialog_layout, null)

        // Find the views in the dialog layout
        val inputLayout = dialogView.findViewById<TextInputLayout>(R.id.dialog_input_layout)
        val inputEditText = dialogView.findViewById<EditText>(R.id.dialog_edit_text)

        inputLayout.hint = hint

        inputEditText.addTextChangedListener {
            inputLayout.error = null
        }

        errorMessageLiveData.observe(lifecycleOwner) {
            inputLayout.error = it
        }

        // Build the dialog
        val builder = AlertDialog.Builder(context)
            .setView(dialogView)
            .setTitle(title)
            .setPositiveButton(positiveButtonText, null)
            .setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
            .setCancelable(false)

        val dialog = builder.create()

        // put positive button listener here, so that I have more control
        // when the dialog is dismissed
        dialog.setOnShowListener {
            val button = (it as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)

            button.setOnClickListener {
                val input = inputEditText.text.toString()

                // Trigger the action
                action(input)
                validationLiveData.observe(lifecycleOwner) { isValid ->
                    if (isValid) {
                        dialog.dismiss()
                    }
                }
            }
        }

        dialog.window?.setBackgroundDrawableResource(R.drawable.rounded_corners)
        dialog.show()
    }
}