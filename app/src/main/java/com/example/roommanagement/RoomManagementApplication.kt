package com.example.roommanagement

import android.app.Application
import com.example.roommanagement.database.UniRoomDatabase
import com.example.roommanagement.database.repository.UniRoomRepository
import com.example.roommanagement.database.repository.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class RoomManagementApplication : Application() {
    private val appScope = CoroutineScope(SupervisorJob())

    private val db by lazy { UniRoomDatabase.getDatabase(this, appScope) }
    val uniRoomRepository by lazy { UniRoomRepository(db.roomDao()) }
    val userRepository by lazy { UserRepository(db.userDao()) }
}