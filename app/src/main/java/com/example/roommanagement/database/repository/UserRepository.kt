package com.example.roommanagement.database.repository

import androidx.annotation.WorkerThread
import com.example.roommanagement.database.dao.UserDao
import com.example.roommanagement.database.entity.User
import kotlinx.coroutines.flow.Flow

class UserRepository(private val userDao: UserDao) {
    @WorkerThread
    suspend fun getByEmail(email: String): User {
        return userDao.getByEmail(email)
    }

    @WorkerThread
    fun getAll(): Flow<List<User>> {
        return userDao.getAll()
    }

    @WorkerThread
    fun getById(id: Long): Flow<User> {
        return userDao.getById(id)
    }

    @WorkerThread
    suspend fun getByIdSuspend(id: Long): User {
        return userDao.getByIdSuspend(id)
    }

    @WorkerThread
    suspend fun update(user: User) {
        userDao.update(user)
    }

    @WorkerThread
    suspend fun delete(user: User) {
        userDao.delete(user)
    }

    @WorkerThread
    suspend fun insert(user: User): Long {
        return userDao.insert(user)
    }

    @WorkerThread
    suspend fun getUserCountWithEmail(email: String): Int {
        return userDao.getUserCountWithEmail(email)
    }
}