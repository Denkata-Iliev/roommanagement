package com.example.roommanagement.database.repository

import androidx.annotation.WorkerThread
import com.example.roommanagement.database.dao.UniRoomDao
import com.example.roommanagement.database.entity.UniRoom
import kotlinx.coroutines.flow.Flow

class UniRoomRepository(private val dao: UniRoomDao) {
    val allRooms: Flow<List<UniRoom>> = dao.getAll()

    @WorkerThread
    fun getRoomCountWithUserId(userId: Long): Flow<Int> {
        return dao.getRoomCountWithUserId(userId)
    }

    @WorkerThread
    fun getById(id: Long): Flow<UniRoom> {
        return dao.getById(id)
    }

    @WorkerThread
    suspend fun insert(uniRoom: UniRoom) {
        dao.insert(uniRoom)
    }

    @WorkerThread
    suspend fun update(uniRoom: UniRoom) {
        dao.update(uniRoom)
    }

    @WorkerThread
    suspend fun delete(uniRoom: UniRoom) {
        dao.delete(uniRoom)
    }

    @WorkerThread
    suspend fun resetRoomsWithUser(userId: Long) {
        dao.resetRoomsWithUser(userId)
    }

    @WorkerThread
    suspend fun existsByRoomNumber(roomNumber: String): Boolean {
        return dao.roomCountWithNumber(roomNumber) == 1
    }

    @WorkerThread
    suspend fun getByIdSuspend(id: Long): UniRoom {
        return dao.getByIdSuspend(id)
    }
}