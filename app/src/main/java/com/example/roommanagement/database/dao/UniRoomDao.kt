package com.example.roommanagement.database.dao

import androidx.room.*
import com.example.roommanagement.database.entity.UniRoom
import kotlinx.coroutines.flow.Flow

@Dao
interface UniRoomDao {

    @Query("SELECT COUNT(id) FROM rooms")
    suspend fun getAllRoomCount(): Int

    @Query("SELECT COUNT(*) FROM rooms WHERE roomNumber = :roomNumber")
    suspend fun roomCountWithNumber(roomNumber: String): Int

    @Query("SELECT COUNT(*) FROM rooms r " +
            "INNER JOIN users u ON r.userId = u.id " +
            "WHERE u.id = :userId")
    fun getRoomCountWithUserId(userId: Long): Flow<Int>

    @Query("SELECT * FROM rooms ORDER BY roomNumber")
    fun getAll(): Flow<List<UniRoom>>

    @Query("SELECT * FROM rooms WHERE id = :id")
    fun getById(id: Long): Flow<UniRoom>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(uniRoom: UniRoom)

    @Update
    suspend fun update(uniRoom: UniRoom)

    @Delete
    suspend fun delete(uniRoom: UniRoom)

    @Query("UPDATE rooms " +
            "SET userId = null, isFree = 1, currentSubject = null " +
            "WHERE userId = :userId")
    suspend fun resetRoomsWithUser(userId: Long)

    @Query("SELECT * FROM rooms WHERE id = :id")
    suspend fun getByIdSuspend(id: Long): UniRoom
}