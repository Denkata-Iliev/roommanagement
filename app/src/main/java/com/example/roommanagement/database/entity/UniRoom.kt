package com.example.roommanagement.database.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(indices = [Index("roomNumber", unique = true)], tableName = "rooms")
data class UniRoom(
    @PrimaryKey(autoGenerate = true)
    val id: Long,

    val roomNumber: String,
    val currentSubject: String? = null,
    val isFree: Boolean,
    val userId: Long? = null
) : Serializable
