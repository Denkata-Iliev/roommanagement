package com.example.roommanagement.database.entity

enum class Role{
    TEACHER,
    ADMIN
}
