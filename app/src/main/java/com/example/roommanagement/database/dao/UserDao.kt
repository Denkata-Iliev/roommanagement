package com.example.roommanagement.database.dao

import androidx.room.*
import com.example.roommanagement.database.entity.User
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {
    @Query("SELECT COUNT(*) FROM users")
    suspend fun getAllUserCount(): Int

    @Query("SELECT COUNT(*) FROM users WHERE email = :email")
    suspend fun getUserCountWithEmail(email: String): Int

    @Query("SELECT * FROM users WHERE email = :email")
    suspend fun getByEmail(email: String): User

    @Query("SELECT * FROM users WHERE id = :id")
    fun getById(id: Long): Flow<User>

    @Query("SELECT * FROM users WHERE id = :id")
    suspend fun getByIdSuspend(id: Long): User

    @Query("SELECT * FROM users")
    fun getAll(): Flow<List<User>>

    @Insert
    suspend fun insert(user: User): Long

    @Update
    suspend fun update(user: User)

    @Delete
    suspend fun delete(user: User)
}