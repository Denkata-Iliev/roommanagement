package com.example.roommanagement.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.roommanagement.database.dao.UniRoomDao
import com.example.roommanagement.database.dao.UserDao
import com.example.roommanagement.database.entity.Role
import com.example.roommanagement.database.entity.UniRoom
import com.example.roommanagement.database.entity.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [User::class, UniRoom::class], version = 3)
abstract class UniRoomDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun roomDao(): UniRoomDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: UniRoomDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): UniRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    UniRoomDatabase::class.java,
                    "uni_rooms_db"
                ).fallbackToDestructiveMigration()
                    .addCallback(DatabaseSeedCallback(scope))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

    private class DatabaseSeedCallback(private val scope: CoroutineScope) :
        RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    populateRoomsTable(database.roomDao())
                    createDefaultAdmin(database.userDao())
                }
            }
        }

        suspend fun populateRoomsTable(dao: UniRoomDao) {
            if (dao.getAllRoomCount() > 0) {
                return
            }

            for (i in 1..9) {
                dao.insert(UniRoom(0, "40$i", isFree = true))
            }
        }

        suspend fun createDefaultAdmin(dao: UserDao) {
            if (dao.getAllUserCount() > 0) {
                return
            }

            dao.insert(
                User(
                    0,
                    "admin@admin.com",
                    "Admin Adminov",
                    "Password123#",
                    Role.ADMIN
                )
            )
        }
    }
}