package com.example.roommanagement.database.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(indices = [Index("email", unique = true)], tableName = "users")
data class User(
    @PrimaryKey(autoGenerate = true)
    val id: Long,

    val email: String,
    val fullname: String,
    val password: String,
    val role: Role
) : Serializable
