package com.example.roommanagement.ui.uniroomrecyclerview

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.roommanagement.ui.viewmodels.UniRoomViewModel
import com.google.android.material.snackbar.Snackbar

class UniRoomItemTouchCallback(private val uniRoomViewModel: UniRoomViewModel) :
    ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val uniRoom = uniRoomViewModel.allRooms.value!![viewHolder.adapterPosition]
        uniRoomViewModel.delete(uniRoom)
        Snackbar.make(
            viewHolder.itemView,
            "Deleted Room #${uniRoom.roomNumber}",
            Snackbar.LENGTH_LONG
        ).setAction("Undo") {
            uniRoomViewModel.insert(uniRoom)
        }
            .show()
    }
}