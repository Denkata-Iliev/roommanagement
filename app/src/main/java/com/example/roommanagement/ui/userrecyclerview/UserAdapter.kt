package com.example.roommanagement.ui.userrecyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.ListAdapter
import com.example.roommanagement.R
import com.example.roommanagement.database.entity.Role
import com.example.roommanagement.database.entity.User
import com.example.roommanagement.ui.viewmodels.UserViewModel
import com.example.roommanagement.util.SharedPreferencesUtil

class UserAdapter(
    diffCallback: UserItemCallback,
    private val userViewModel: UserViewModel
) : ListAdapter<User, UserVH>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_layout, parent, false)
        return UserVH(view)
    }

    override fun onBindViewHolder(holder: UserVH, position: Int) {
        val user = getItem(position)

        holder.userEmail.text = user.email
        holder.userFullName.text = user.fullname
        holder.userRole.text = user.role.name

        holder.itemView.setOnClickListener {

            val currentUserId = SharedPreferencesUtil.getSharedPrefs(it.context).getUserId()
            if (user.id == currentUserId) {
                Toast.makeText(
                    it.context,
                    "You can't promote/demote yourself!",
                    Toast.LENGTH_SHORT
                ).show()

                return@setOnClickListener
            }

            AlertDialog.Builder(it.context)
                .setTitle("Promote '${user.fullname}'")
                .setMessage("Promote or demote ${user.fullname}?")
                .setPositiveButton("Promote") { dialog, which ->
                    if (user.role == Role.ADMIN) {
                        Toast.makeText(it.context, "User is already an ADMIN", Toast.LENGTH_SHORT)
                            .show()

                        dialog.dismiss()
                    }

                    userViewModel.changeUserRole(user, Role.ADMIN)
                }
                .setNegativeButton("Demote") { dialog, _ ->
                    if (user.role == Role.TEACHER) {
                        Toast.makeText(it.context, "User is already a TEACHER", Toast.LENGTH_SHORT)
                            .show()

                        dialog.dismiss()
                    }

                    userViewModel.changeUserRole(user, Role.TEACHER)
                }
                .setNeutralButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .show()
        }
    }
}