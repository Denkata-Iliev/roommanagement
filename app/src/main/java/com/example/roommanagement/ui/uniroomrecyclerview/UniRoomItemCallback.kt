package com.example.roommanagement.ui.uniroomrecyclerview

import androidx.recyclerview.widget.DiffUtil.ItemCallback
import com.example.roommanagement.database.entity.UniRoom

class UniRoomItemCallback : ItemCallback<UniRoom>() {
    override fun areItemsTheSame(oldItem: UniRoom, newItem: UniRoom): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: UniRoom, newItem: UniRoom): Boolean {
        return oldItem.roomNumber == newItem.roomNumber
    }
}