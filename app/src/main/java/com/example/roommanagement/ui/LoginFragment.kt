package com.example.roommanagement.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.roommanagement.RoomManagementApplication
import com.example.roommanagement.databinding.FragmentLoginBinding
import com.example.roommanagement.ui.viewmodels.UserViewModel
import com.example.roommanagement.ui.viewmodels.UserViewModelFactory
import com.example.roommanagement.util.SharedPreferencesUtil

class LoginFragment : BaseFragment() {
    private lateinit var binding: FragmentLoginBinding
    private lateinit var activity: FragmentActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        val context = requireContext()

        val userId = SharedPreferencesUtil.getSharedPrefs(context).getUserId()

        if (userId > 0) {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToMainFragment())
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity = requireActivity()

        val userViewModel: UserViewModel by viewModels {
            UserViewModelFactory((activity.application as RoomManagementApplication).userRepository)
        }

        binding.edtPasswordLogin.addTextChangedListener {
            binding.passwordLoginLayout.error = null
            binding.errorMsg.visibility = View.GONE
        }

        binding.edtEmailLogin.addTextChangedListener {
            binding.emailLoginLayout.error = null
            binding.errorMsg.visibility = View.GONE
        }

        userViewModel.emailErrorMessage.observe(viewLifecycleOwner) {
            binding.emailLoginLayout.error = it
        }

        userViewModel.passwordErrorMessage.observe(viewLifecycleOwner) {
            binding.passwordLoginLayout.error = it
        }

        userViewModel.generalErrorMessage.observe(viewLifecycleOwner) {
            binding.errorMsg.visibility = View.VISIBLE
            binding.errorMsg.text = it
        }

        binding.loginBtn.setOnClickListener {
            val email = binding.edtEmailLogin.text.toString()
            val password = binding.edtPasswordLogin.text.toString()

            userViewModel.loginUser(email, password, activity)
        }

        userViewModel.dataValid.observe(viewLifecycleOwner) {
            if (it) {
                findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToMainFragment())
            }
        }

        binding.registerTxt.setOnClickListener {
            goToRegister(it)
        }
    }

    private fun goToRegister(view: View) {
        Navigation.findNavController(view).navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
    }
}