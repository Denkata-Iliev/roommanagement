package com.example.roommanagement.ui.uniroomrecyclerview

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.roommanagement.R

class UniRoomVH(view: View) : RecyclerView.ViewHolder(view) {
    val roomNumber: TextView = view.findViewById(R.id.room_number)
    val personInside: TextView = view.findViewById(R.id.person_inside)
    val isFree: TextView = view.findViewById(R.id.is_free)
}