package com.example.roommanagement.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

abstract class BaseFragment : Fragment() {
    override fun onResume() {
        super.onResume()

        val navController = findNavController()
        val navBackStackEntry = navController.currentBackStackEntry
        val label = navBackStackEntry?.destination?.label

        (requireActivity() as AppCompatActivity).supportActionBar?.title = label
    }
}