package com.example.roommanagement.ui.userrecyclerview

import androidx.recyclerview.widget.DiffUtil.ItemCallback
import com.example.roommanagement.database.entity.User

class UserItemCallback : ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.email == newItem.email
    }
}