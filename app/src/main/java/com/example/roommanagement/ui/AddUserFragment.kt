package com.example.roommanagement.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.roommanagement.RoomManagementApplication
import com.example.roommanagement.database.entity.Role
import com.example.roommanagement.databinding.FragmentAddUserBinding
import com.example.roommanagement.ui.viewmodels.UserViewModel
import com.example.roommanagement.ui.viewmodels.UserViewModelFactory

class AddUserFragment : BaseFragment() {
    private lateinit var binding: FragmentAddUserBinding
    private lateinit var activity: FragmentActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity = requireActivity()

        val userViewModel: UserViewModel by viewModels {
            UserViewModelFactory((activity.application as RoomManagementApplication).userRepository)
        }

        userViewModel.emailErrorMessage.observe(viewLifecycleOwner) {
            binding.emailLayout.error = it
        }

        userViewModel.fullNameErrorMessage.observe(viewLifecycleOwner) {
            binding.fullnameLayout.error = it
        }

        userViewModel.passwordErrorMessage.observe(viewLifecycleOwner) {
            binding.passwordLayout.error = it
        }

        userViewModel.confPassErrorMessage.observe(viewLifecycleOwner) {
            binding.confPassLayout.error = it
        }

        addInputTextChangedListeners()

        binding.createUserBtn.setOnClickListener {
            val emailText = binding.edtEmail.text.toString()
            val fullNameText = binding.edtFullname.text.toString()
            val passwordText = binding.edtPassword.text.toString()
            val confPassText = binding.edtConfPass.text.toString()

            userViewModel.createNewUser(
                emailText,
                fullNameText,
                passwordText,
                confPassText,
                Role.ADMIN
            )
        }

        userViewModel.dataValid.observe(viewLifecycleOwner) { isValid ->
            if (isValid) {
                findNavController().popBackStack()
            }
        }

        binding.close.setOnClickListener {
            Navigation.findNavController(it).popBackStack()
        }
    }

    private fun addInputTextChangedListeners() {
        binding.edtEmail.addTextChangedListener {
            binding.emailLayout.error = null
        }

        binding.edtFullname.addTextChangedListener {
            binding.fullnameLayout.error = null
        }

        binding.edtPassword.addTextChangedListener {
            binding.passwordLayout.error = null
        }

        binding.edtConfPass.addTextChangedListener {
            binding.confPassLayout.error = null
        }
    }
}