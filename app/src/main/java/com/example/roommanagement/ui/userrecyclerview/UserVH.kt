package com.example.roommanagement.ui.userrecyclerview

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.roommanagement.R

class UserVH(itemView: View) : ViewHolder(itemView) {
    val userEmail = itemView.findViewById<TextView>(R.id.user_email)
    val userFullName = itemView.findViewById<TextView>(R.id.user_fullname)
    val userRole = itemView.findViewById<TextView>(R.id.user_role)
}