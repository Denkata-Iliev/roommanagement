package com.example.roommanagement.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.roommanagement.R
import com.example.roommanagement.RoomManagementApplication
import com.example.roommanagement.database.entity.Role
import com.example.roommanagement.databinding.FragmentMainBinding
import com.example.roommanagement.ui.uniroomrecyclerview.UniRoomAdapter
import com.example.roommanagement.ui.uniroomrecyclerview.UniRoomItemCallback
import com.example.roommanagement.ui.uniroomrecyclerview.UniRoomItemTouchCallback
import com.example.roommanagement.ui.viewmodels.UniRoomViewModel
import com.example.roommanagement.ui.viewmodels.UniRoomViewModelFactory
import com.example.roommanagement.ui.viewmodels.UserViewModel
import com.example.roommanagement.ui.viewmodels.UserViewModelFactory
import com.example.roommanagement.util.DialogUtils
import com.example.roommanagement.util.SharedPreferencesUtil

class MainFragment : BaseFragment(), MenuProvider {
    private lateinit var binding: FragmentMainBinding
    private lateinit var activity: FragmentActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val menuHost: MenuHost = requireActivity()

        // Add the MenuProvider to the MenuHost
        menuHost.addMenuProvider(
            this, // your Fragment implements MenuProvider, so we use this here
            viewLifecycleOwner, // Only show the Menu when your Fragment's View exists
            Lifecycle.State.RESUMED // And when the Fragment is RESUMED
        )

        activity = requireActivity()

        val uniRoomViewModel: UniRoomViewModel by viewModels {
            UniRoomViewModelFactory((activity.application as RoomManagementApplication).uniRoomRepository)
        }

        val userViewModel: UserViewModel by viewModels {
            UserViewModelFactory((activity.application as RoomManagementApplication).userRepository)
        }

        val uniRoomAdapter =
            UniRoomAdapter(UniRoomItemCallback(), userViewModel, viewLifecycleOwner)
        val linearLayoutManager = LinearLayoutManager(context)

        uniRoomViewModel.allRooms.observe(viewLifecycleOwner) {
            uniRoomAdapter.submitList(it)
        }

        binding.roomListRV.apply {
            adapter = uniRoomAdapter
            layoutManager = linearLayoutManager
        }

        val role = SharedPreferencesUtil.getSharedPrefs(activity).getUserRole()
        if (role == Role.ADMIN.name) {
            ItemTouchHelper(UniRoomItemTouchCallback(uniRoomViewModel)).attachToRecyclerView(binding.roomListRV)
            binding.addRoomBtn.visibility = View.VISIBLE
        }

        binding.addRoomBtn.setOnClickListener {
            DialogUtils.showDialogBox(
                requireContext(),
                "Add Room",
                "Room Number",
                "Create",
                uniRoomViewModel.dataValid,
                uniRoomViewModel.errorMessage,
                viewLifecycleOwner
            ) { input ->
                uniRoomViewModel.insertWithRoomNumber(input)
            }
        }
    }

    override fun onPrepareMenu(menu: Menu) {
        val users = menu.findItem(R.id.users)
        val userRole = SharedPreferencesUtil.getSharedPrefs(activity).getUserRole()

        if (userRole == Role.ADMIN.name) {
            users.isVisible = true
        }
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.main_menu, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.logout -> {
                SharedPreferencesUtil.getSharedPrefs(activity).logout()

                findNavController().navigate(R.id.loginFragment)
            }

            R.id.profile -> {
                findNavController().navigate(R.id.profileFragment)
            }

            R.id.users -> {
                findNavController().navigate(R.id.userListFragment)
            }
        }

        return true
    }
}