package com.example.roommanagement.ui.viewmodels

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.roommanagement.database.entity.UniRoom
import com.example.roommanagement.database.repository.UniRoomRepository
import com.example.roommanagement.util.SharedPreferencesUtil
import kotlinx.coroutines.launch

class UniRoomViewModel(private val repository: UniRoomRepository) : ViewModel() {
    private val _errorMessage = MutableLiveData<String>()
    private val _dataValid = MutableLiveData<Boolean>()

    val allRooms: LiveData<List<UniRoom>> = repository.allRooms.asLiveData()
    val errorMessage: LiveData<String> = _errorMessage
    val dataValid: LiveData<Boolean> = _dataValid

    fun getRoomCountWithUserId(userId: Long): LiveData<Int> {
        return repository.getRoomCountWithUserId(userId).asLiveData()
    }

    fun getById(id: Long): LiveData<UniRoom> {
        return repository.getById(id).asLiveData()
    }

    private fun takeRoom(room: UniRoom, currentSubject: String, context: Context) {
        viewModelScope.launch {
            if (currentSubject.isBlank() || currentSubject.length < 3) {
                _errorMessage.value = "Current Subject must be at least 3 characters long"
                setDataValid(false)

                return@launch
            }

            val userId = SharedPreferencesUtil.getSharedPrefs(context).getUserId()

            update(
                UniRoom(
                    room.id,
                    room.roomNumber,
                    currentSubject,
                    false,
                    userId
                )
            )

            setDataValid(true)
        }
    }

    fun takeRoom(roomId: Long, currentSubject: String, context: Context) {
        viewModelScope.launch {
            val room = getByIdSuspend(roomId)
            takeRoom(room, currentSubject, context)
        }
    }

    fun insertWithRoomNumber(roomNumber: String) {
        viewModelScope.launch {
            if (roomNumber.isBlank() || roomNumber.length < 3 || roomNumber.length > 5) {
                _errorMessage.value = "Room Number must be between 3 and 5 characters"
                setDataValid(false)
                return@launch
            }

            if (existsByRoomNumber(roomNumber)) {
                _errorMessage.value = "This Room Number is already taken"
                setDataValid(false)
                return@launch
            }

            repository.insert(
                UniRoom(
                    0,
                    roomNumber,
                    isFree = true
                )
            )

            setDataValid(true)
        }
    }

    fun insert(uniRoom: UniRoom) {
        viewModelScope.launch {
            repository.insert(uniRoom)
        }
    }

    fun update(uniRoom: UniRoom) {
        viewModelScope.launch {
            repository.update(uniRoom)
        }
    }

    fun delete(uniRoom: UniRoom) {
        viewModelScope.launch {
            repository.delete(uniRoom)
        }
    }

    fun resetRoomsWithUser(userId: Long) {
        viewModelScope.launch {
            repository.resetRoomsWithUser(userId)
        }
    }

    private suspend fun existsByRoomNumber(roomNumber: String): Boolean {
        return repository.existsByRoomNumber(roomNumber)
    }

    private suspend fun getByIdSuspend(id: Long): UniRoom {
        return repository.getByIdSuspend(id)
    }

    private fun setDataValid(isValid: Boolean) {
        _dataValid.value = isValid
    }
}

class UniRoomViewModelFactory(private val repository: UniRoomRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UniRoomViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return UniRoomViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}