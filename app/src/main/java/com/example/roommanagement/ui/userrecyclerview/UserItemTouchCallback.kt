package com.example.roommanagement.ui.userrecyclerview

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.roommanagement.ui.viewmodels.UniRoomViewModel
import com.example.roommanagement.ui.viewmodels.UserViewModel
import com.google.android.material.snackbar.Snackbar

class UserItemTouchCallback(
    private val userViewModel: UserViewModel,
    private val uniRoomViewModel: UniRoomViewModel,
    private val currentUserId: Long
) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {

        val user = userViewModel.allUsers.value!![viewHolder.adapterPosition]
        if (user.id == currentUserId) {
            Snackbar.make(
                viewHolder.itemView,
                "You can't delete yourself!",
                Snackbar.LENGTH_LONG
            ).show()

            return 0
        }

        return super.getMovementFlags(recyclerView, viewHolder)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val user = userViewModel.allUsers.value!![viewHolder.adapterPosition]

        userViewModel.delete(user)
        uniRoomViewModel.resetRoomsWithUser(user.id)
        Snackbar.make(
            viewHolder.itemView,
            "Deleted User '${user.email}'",
            Snackbar.LENGTH_LONG
        ).setAction("Undo") {
            userViewModel.insert(user)
        }
            .show()
    }
}