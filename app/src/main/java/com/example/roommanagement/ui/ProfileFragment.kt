package com.example.roommanagement.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.roommanagement.R
import com.example.roommanagement.RoomManagementApplication
import com.example.roommanagement.database.entity.Role
import com.example.roommanagement.databinding.FragmentProfileBinding
import com.example.roommanagement.ui.viewmodels.UniRoomViewModel
import com.example.roommanagement.ui.viewmodels.UniRoomViewModelFactory
import com.example.roommanagement.ui.viewmodels.UserViewModel
import com.example.roommanagement.ui.viewmodels.UserViewModelFactory
import com.example.roommanagement.util.SharedPreferencesUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.squareup.picasso.Picasso


class ProfileFragment : BaseFragment(), MenuProvider {
    private lateinit var binding: FragmentProfileBinding
    private lateinit var activity: FragmentActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val menuHost: MenuHost = requireActivity()

        // Add the MenuProvider to the MenuHost
        menuHost.addMenuProvider(
            this, // your Fragment implements MenuProvider, so we use this here
            viewLifecycleOwner, // Only show the Menu when your Fragment's View exists
            Lifecycle.State.RESUMED // And when the Fragment is RESUMED
        )

        activity = requireActivity()

        val userViewModel: UserViewModel by viewModels {
            UserViewModelFactory((activity.application as RoomManagementApplication).userRepository)
        }

        val uniRoomViewModel: UniRoomViewModel by viewModels {
            UniRoomViewModelFactory((activity.application as RoomManagementApplication).uniRoomRepository)
        }

        val userId = SharedPreferencesUtil.getSharedPrefs(activity).getUserId()

        userViewModel.getById(userId).observe(viewLifecycleOwner) { user ->
            binding.emailTxt.text = user.email
            binding.fullnameTxt.text = user.fullname
            binding.roleTxt.text = user.role.name

            Picasso.get()
                .load("https://ui-avatars.com/api/?rounded=true&size=256&format=png&name=${user.fullname}")
                .error(R.drawable.baseline_account_box_24)
                .into(binding.pfp)

            binding.editBtn.setOnClickListener {
                Navigation.findNavController(it)
                    .navigate(ProfileFragmentDirections.actionProfileFragmentToEditProfileFragment(user))
            }
        }

        binding.deleteBtn.setOnClickListener {
            createAndShowDialog(activity, userViewModel, uniRoomViewModel, userId, it)
        }

        binding.backBtn.setOnClickListener {
            Navigation.findNavController(it).popBackStack()
        }
    }

    private fun createAndShowDialog(
        activity: FragmentActivity,
        userViewModel: UserViewModel,
        uniRoomViewModel: UniRoomViewModel,
        userId: Long,
        view: View
    ) {
        val builder = MaterialAlertDialogBuilder(activity)
            .setMessage("Are you sure you want to delete your account?")
            .setPositiveButton("YES") { dialog, _ ->

                userViewModel.deleteAndLogout(userId, activity)
                uniRoomViewModel.resetRoomsWithUser(userId)
                dialog.dismiss()

                Navigation.findNavController(view).navigate(R.id.loginFragment)
            }
            .setNegativeButton("NO") { dialog, _ ->
                dialog.dismiss()
            }

        builder.create().show()
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.main_menu, menu)
    }

    override fun onPrepareMenu(menu: Menu) {
        menu.findItem(R.id.rooms).isVisible = true
        menu.findItem(R.id.profile).isVisible = false

        val users = menu.findItem(R.id.users)
        val userRole = SharedPreferencesUtil.getSharedPrefs(activity).getUserRole()

        if (userRole == Role.ADMIN.name) {
            users.isVisible = true
        }
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.logout -> {
                SharedPreferencesUtil.getSharedPrefs(activity).logout()

                findNavController().navigate(R.id.loginFragment)
            }

            R.id.rooms -> {
                findNavController().navigate(R.id.mainFragment)
            }

            R.id.users -> {
                findNavController().navigate(R.id.userListFragment)
            }
        }

        return true
    }
}