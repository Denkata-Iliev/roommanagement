package com.example.roommanagement.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.example.roommanagement.RoomManagementApplication
import com.example.roommanagement.databinding.FragmentEditProfileBinding
import com.example.roommanagement.ui.viewmodels.UserViewModel
import com.example.roommanagement.ui.viewmodels.UserViewModelFactory

class EditProfileFragment : BaseFragment() {
    private lateinit var binding: FragmentEditProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEditProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val args: EditProfileFragmentArgs by navArgs()
        val user = args.user
        val activity = requireActivity()

        val userViewModel: UserViewModel by viewModels {
            UserViewModelFactory((activity.application as RoomManagementApplication).userRepository)
        }

        addInputTextChangedListeners()

        binding.edtEmail.setText(user.email)
        binding.edtFullname.setText(user.fullname)

        userViewModel.emailErrorMessage.observe(viewLifecycleOwner) {
            binding.emailLayout.error = it
        }

        userViewModel.fullNameErrorMessage.observe(viewLifecycleOwner) {
            binding.fullnameLayout.error = it
        }

        userViewModel.oldPasswordErrorMessage.observe(viewLifecycleOwner) {
            binding.oldPasswordLayout.error = it
        }

        userViewModel.passwordErrorMessage.observe(viewLifecycleOwner) {
            binding.newPasswordLayout.error = it
        }

        binding.updateBtn.setOnClickListener {
            val emailText = binding.edtEmail.text.toString()
            val fullNameText = binding.edtFullname.text.toString()
            val oldPasswordText = binding.edtOldPassword.text.toString()
            val newPasswordText = binding.edtNewPass.text.toString()

            userViewModel.editUser(emailText, fullNameText, user, oldPasswordText, newPasswordText)

            userViewModel.dataValid.observe(viewLifecycleOwner) { isValid ->
                if (isValid) {
                    goBack(it)
                }
            }
        }

        binding.close.setOnClickListener {
            goBack(it)
        }
    }

    private fun goBack(it: View) {
        Navigation.findNavController(it).popBackStack()
    }

    private fun addInputTextChangedListeners() {
        binding.edtEmail.addTextChangedListener {
            binding.emailLayout.error = null
        }

        binding.edtFullname.addTextChangedListener {
            binding.fullnameLayout.error = null
        }

        binding.edtOldPassword.addTextChangedListener {
            binding.oldPasswordLayout.error = null
        }

        binding.edtNewPass.addTextChangedListener {
            binding.newPasswordLayout.error = null
        }
    }
}