package com.example.roommanagement.ui.uniroomrecyclerview

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.ListAdapter
import com.example.roommanagement.R
import com.example.roommanagement.database.entity.UniRoom
import com.example.roommanagement.ui.MainFragmentDirections
import com.example.roommanagement.ui.viewmodels.UserViewModel

class UniRoomAdapter(
    diffCallback: ItemCallback<UniRoom>,
    private val userViewModel: UserViewModel,
    private val lifecycleOwner: LifecycleOwner
) : ListAdapter<UniRoom, UniRoomVH>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UniRoomVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.room_layout, parent, false)
        return UniRoomVH(view)
    }

    override fun onBindViewHolder(holder: UniRoomVH, position: Int) {
        val room = getItem(position)
        val context = holder.isFree.context

        setRoomViewHolderProperties(room, holder, context)

        holder.itemView.setOnClickListener {
            val controller = Navigation.findNavController(it)
            controller.navigate(MainFragmentDirections.actionMainFragmentToDetailsFragment(room.id))
        }
    }

    private fun setRoomViewHolderProperties(uniRoom: UniRoom, holder: UniRoomVH, context: Context) {
        setIsFreeTextColor(uniRoom, holder, context)

        holder.isFree.text =
            if (uniRoom.isFree) context.getString(R.string.free) else context.getString(R.string.taken)

        holder.roomNumber.text = uniRoom.roomNumber

        if (uniRoom.userId != null) {
            userViewModel.getById(uniRoom.userId).observe(lifecycleOwner) {
                holder.personInside.text = it.fullname
            }
        }
    }

    private fun setIsFreeTextColor(uniRoom: UniRoom, holder: UniRoomVH, context: Context) {
        if (uniRoom.isFree) {
            holder.isFree.setTextColor(context.getColor(R.color.free))
        } else {
            holder.isFree.setTextColor(context.getColor(R.color.taken))
        }
    }
}