package com.example.roommanagement.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.roommanagement.R
import com.example.roommanagement.RoomManagementApplication
import com.example.roommanagement.databinding.FragmentUserListBinding
import com.example.roommanagement.ui.userrecyclerview.UserAdapter
import com.example.roommanagement.ui.userrecyclerview.UserItemCallback
import com.example.roommanagement.ui.userrecyclerview.UserItemTouchCallback
import com.example.roommanagement.ui.viewmodels.UniRoomViewModel
import com.example.roommanagement.ui.viewmodels.UniRoomViewModelFactory
import com.example.roommanagement.ui.viewmodels.UserViewModel
import com.example.roommanagement.ui.viewmodels.UserViewModelFactory
import com.example.roommanagement.util.SharedPreferencesUtil

class UserListFragment : BaseFragment(), MenuProvider {
    private lateinit var binding: FragmentUserListBinding
    private lateinit var activity: FragmentActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val menuHost: MenuHost = requireActivity()

        // Add the MenuProvider to the MenuHost
        menuHost.addMenuProvider(
            this, // your Fragment implements MenuProvider, so we use this here
            viewLifecycleOwner, // Only show the Menu when your Fragment's View exists
            Lifecycle.State.RESUMED // And when the Fragment is RESUMED
        )

        val uniRoomViewModel: UniRoomViewModel by viewModels {
            UniRoomViewModelFactory((activity.application as RoomManagementApplication).uniRoomRepository)
        }

        val userViewModel: UserViewModel by viewModels {
            UserViewModelFactory((activity.application as RoomManagementApplication).userRepository)
        }

        activity = requireActivity()

        val userAdapter = UserAdapter(UserItemCallback(), userViewModel)
        val linearLayoutManager = LinearLayoutManager(context)

        userViewModel.allUsers.observe(viewLifecycleOwner) {
            userAdapter.submitList(it)
        }

        binding.userListRV.apply {
            adapter = userAdapter
            layoutManager = linearLayoutManager
        }

        val currentUserId = SharedPreferencesUtil.getSharedPrefs(activity).getUserId()
        ItemTouchHelper(UserItemTouchCallback(userViewModel, uniRoomViewModel, currentUserId))
            .attachToRecyclerView(binding.userListRV)

        binding.addUserBtn.setOnClickListener {
            Navigation.findNavController(it)
                .navigate(UserListFragmentDirections.actionUserListFragmentToAddUserFragment())
        }
    }

    override fun onPrepareMenu(menu: Menu) {
        menu.findItem(R.id.rooms).isVisible = true
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.main_menu, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.logout -> {
                SharedPreferencesUtil.getSharedPrefs(activity).logout()

                findNavController().navigate(R.id.loginFragment)
            }

            R.id.profile -> {
                findNavController().navigate(R.id.profileFragment)
            }

            R.id.rooms -> {
                findNavController().navigate(R.id.mainFragment)
            }
        }

        return true
    }
}