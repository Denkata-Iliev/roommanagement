package com.example.roommanagement.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.roommanagement.R
import com.example.roommanagement.RoomManagementApplication
import com.example.roommanagement.database.entity.Role
import com.example.roommanagement.database.entity.UniRoom
import com.example.roommanagement.databinding.FragmentDetailsBinding
import com.example.roommanagement.ui.viewmodels.UniRoomViewModel
import com.example.roommanagement.ui.viewmodels.UniRoomViewModelFactory
import com.example.roommanagement.ui.viewmodels.UserViewModel
import com.example.roommanagement.ui.viewmodels.UserViewModelFactory
import com.example.roommanagement.util.DialogUtils
import com.example.roommanagement.util.SharedPreferencesUtil

class DetailsFragment : BaseFragment(), MenuProvider {
    private lateinit var binding: FragmentDetailsBinding
    private lateinit var activity: FragmentActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val menuHost: MenuHost = requireActivity()

        // Add the MenuProvider to the MenuHost
        menuHost.addMenuProvider(
            this, // your Fragment implements MenuProvider, so we use this here
            viewLifecycleOwner, // Only show the Menu when your Fragment's View exists
            Lifecycle.State.RESUMED // And when the Fragment is RESUMED
        )

        activity = requireActivity()
        val context = requireContext()

        val currentUserId = SharedPreferencesUtil.getSharedPrefs(activity).getUserId()

        val currentUserRole = SharedPreferencesUtil.getSharedPrefs(activity).getUserRole()

        val userViewModel: UserViewModel by viewModels {
            UserViewModelFactory((activity.application as RoomManagementApplication).userRepository)
        }

        val uniRoomViewModel: UniRoomViewModel by viewModels {
            UniRoomViewModelFactory((activity.application as RoomManagementApplication).uniRoomRepository)
        }

        val args: DetailsFragmentArgs by navArgs()
        val roomId = args.roomId

        // if user has taken a room, 'Take' button won't show in other rooms
        uniRoomViewModel.getRoomCountWithUserId(currentUserId).observe(viewLifecycleOwner) {
            if (it >= 1) {
                binding.takeBtn.visibility = View.GONE
            }
        }

        uniRoomViewModel.getById(roomId).observe(viewLifecycleOwner) { uniRoom ->

            // if room isn't free, 'Take' button won't show
            // if it is, 'Dismiss' button won't show
            if (!uniRoom.isFree) {
                binding.takeBtn.visibility = View.GONE
            } else {
                binding.dismissBtn.visibility = View.GONE
            }

            // if same user or admin, 'Dismiss' button shows
            if (!uniRoom.isFree && (uniRoom.userId == currentUserId || currentUserRole == Role.ADMIN.name)) {
                binding.dismissBtn.visibility = View.VISIBLE
            }

            binding.roomNumber.text = uniRoom.roomNumber
            binding.currentSubject.text = uniRoom.currentSubject ?: getString(R.string.not_available)

            if (uniRoom.userId != null) {
                userViewModel.getById(uniRoom.userId).observe(viewLifecycleOwner) {
                    binding.personInsideTxt.text = it.fullname
                }
            } else {
                binding.personInsideTxt.text = getString(R.string.not_available)
            }

            binding.dismissBtn.setOnClickListener {
                uniRoomViewModel.update(
                    UniRoom(
                        uniRoom.id,
                        uniRoom.roomNumber,
                        null,
                        true,
                        null
                    )
                )

                // after room is freed, show 'Take' button
                binding.takeBtn.visibility = View.VISIBLE
            }
        }

        binding.takeBtn.setOnClickListener {
            DialogUtils.showDialogBox(
                context,
                "Take Room",
                "Current subject",
                "Take",
                uniRoomViewModel.dataValid,
                uniRoomViewModel.errorMessage,
                viewLifecycleOwner
            ) { input ->
                uniRoomViewModel.takeRoom(roomId, input, context)
            }
        }

        binding.backBtn.setOnClickListener {
            goBack(it)
        }
    }

    private fun goBack(view: View) {
        Navigation.findNavController(view).popBackStack()
    }

    override fun onPrepareMenu(menu: Menu) {
        val users = menu.findItem(R.id.users)
        val userRole = SharedPreferencesUtil.getSharedPrefs(activity).getUserRole()

        if (userRole == Role.ADMIN.name) {
            users.isVisible = true
        }
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.main_menu, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.logout -> {
                SharedPreferencesUtil.getSharedPrefs(activity).logout()

                findNavController().navigate(R.id.loginFragment)
            }

            R.id.profile -> {
                findNavController().navigate(R.id.profileFragment)
            }

            R.id.users -> {
                findNavController().navigate(R.id.userListFragment)
            }
        }

        return true
    }
}