package com.example.roommanagement.ui.viewmodels

import android.content.Context
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.roommanagement.database.entity.Role
import com.example.roommanagement.database.entity.User
import com.example.roommanagement.database.repository.UserRepository
import com.example.roommanagement.util.SharedPreferencesUtil
import kotlinx.coroutines.launch
import java.util.regex.Pattern

private const val INCORRECT_EMAIL_OR_PASSWORD = "Incorrect email or password"

class UserViewModel(private val repository: UserRepository) : ViewModel() {
    private val _emailErrorMessage = MutableLiveData<String>()
    private val _fullNameErrorMessage = MutableLiveData<String>()
    private val _oldPasswordErrorMessage = MutableLiveData<String>()
    private val _passwordErrorMessage = MutableLiveData<String>()
    private val _confPassErrorMessage = MutableLiveData<String>()
    private val _generalErrorMessage = MutableLiveData<String>()
    private val _dataValid = MutableLiveData<Boolean>()

    val emailErrorMessage: LiveData<String> = _emailErrorMessage
    val fullNameErrorMessage: LiveData<String> = _fullNameErrorMessage
    val passwordErrorMessage: LiveData<String> = _passwordErrorMessage
    val oldPasswordErrorMessage: LiveData<String> = _oldPasswordErrorMessage
    val confPassErrorMessage: LiveData<String> = _confPassErrorMessage
    val generalErrorMessage: LiveData<String> = _generalErrorMessage
    val dataValid: LiveData<Boolean> = _dataValid
    val allUsers: LiveData<List<User>> = repository.getAll().asLiveData()

    fun getById(id: Long): LiveData<User> {
        return repository.getById(id).asLiveData()
    }

    fun delete(user: User) {
        viewModelScope.launch {
            repository.delete(user)
        }
    }

    fun deleteAndLogout(userId: Long, context: Context) {
        viewModelScope.launch {
            delete(getByIdSuspend(userId))
            SharedPreferencesUtil.getSharedPrefs(context).logout()
        }
    }

    fun editUser(
        email: String,
        fullName: String,
        user: User,
        oldPassword: String,
        newPassword: String
    ) {
        viewModelScope.launch {
            if (!isEmailValid(email)) {
                return@launch
            }

            if (existsByEmail(email) && user.email != email) {
                _emailErrorMessage.value = "This email is already taken"
                setDataValid(false)

                return@launch
            }

            if (!isFullNameValid(fullName)) {
                return@launch
            }

            if (user.password != oldPassword) {
                _oldPasswordErrorMessage.value = "Old password doesn't match"
                setDataValid(false)

                return@launch
            }

            if (!isPasswordValid(newPassword)) {
                return@launch
            }

            update(
                User(
                    user.id,
                    email,
                    fullName,
                    newPassword,
                    user.role
                )
            )

            setDataValid(true)
        }
    }

    fun changeUserRole(user: User, role: Role) {
        viewModelScope.launch {
            update(
                User(
                    user.id,
                    user.email,
                    user.fullname,
                    user.password,
                    role
                )
            )
        }
    }

    fun loginUser(email: String, password: String, context: Context) {
        viewModelScope.launch {
            if (email.isBlank()) {
                _emailErrorMessage.value = "Email field is required"
                setDataValid(false)

                return@launch
            }

            if (password.isBlank()) {
                _passwordErrorMessage.value = "Password field is required"
                setDataValid(false)

                return@launch
            }

            if (!existsByEmail(email)) {
                _generalErrorMessage.value = INCORRECT_EMAIL_OR_PASSWORD
                setDataValid(false)

                return@launch
            }

            val user = getByEmail(email)

            if (user.password != password) {
                _generalErrorMessage.value = INCORRECT_EMAIL_OR_PASSWORD
                setDataValid(false)

                return@launch
            }

            SharedPreferencesUtil.getSharedPrefs(context).saveUserInfo(user.id, user.role)

            setDataValid(true)
        }
    }

    fun registerUser(
        email: String,
        fullName: String,
        password: String,
        confPass: String,
        context: Context,
        role: Role
    ) {
        viewModelScope.launch {
            if (!areInputsValid(email, fullName, password, confPass)) {
                return@launch
            }

            if (existsByEmail(email)) {
                setErrorMessage(_emailErrorMessage, "This email is already taken")
                setDataValid(false)

                return@launch
            }

            val userId = insertSuspend(
                User(
                    0,
                    email,
                    fullName,
                    password,
                    role
                )
            )

            SharedPreferencesUtil
                .getSharedPrefs(context)
                .saveUserInfo(userId, role)

            setDataValid(true)
        }
    }

    fun createNewUser(
        email: String,
        fullName: String,
        password: String,
        confPass: String,
        role: Role
    ) {
        viewModelScope.launch {
            if (!areInputsValid(email, fullName, password, confPass)) {
                return@launch
            }

            if (existsByEmail(email)) {
                setDataValid(false)

                setErrorMessage(_emailErrorMessage, "This email is already taken")

                return@launch
            }

            insertSuspend(
                User(
                    0,
                    email,
                    fullName,
                    password,
                    role
                )
            )

            setDataValid(true)
        }
    }

    fun insert(user: User) {
        viewModelScope.launch {
            repository.insert(user)
        }
    }

    private suspend fun update(user: User) {
        repository.update(user)
    }

    private suspend fun insertSuspend(user: User): Long {
        return repository.insert(user)
    }

    private suspend fun getByIdSuspend(id: Long): User {
        return repository.getByIdSuspend(id)
    }

    private suspend fun getByEmail(email: String): User {
        return repository.getByEmail(email)
    }

    private suspend fun existsByEmail(email: String): Boolean {
        return repository.getUserCountWithEmail(email) >= 1
    }

    private fun setDataValid(isValid: Boolean) {
        _dataValid.value = isValid
    }

    private fun setErrorMessage(liveData: MutableLiveData<String>, errorMessage: String) {
        liveData.value = errorMessage
    }

    private fun areInputsValid(
        email: String,
        fullName: String,
        password: String,
        confPass: String
    ): Boolean {
        if (!isEmailValid(email)) {
            return false
        }

        if (!isFullNameValid(fullName)) {
            return false
        }

        if (!isPasswordValid(password)) {
            return false
        }

        if (confPass != password) {
            setDataValid(false)

            setErrorMessage(_confPassErrorMessage, "Passwords don't match")
            return false
        }

        return true
    }

    private fun isFullNameValid(fullName: String): Boolean {
        if (fullName.isBlank() || fullName.length < 3) {
            setDataValid(false)

            setErrorMessage(_fullNameErrorMessage, "Full name must be at least 3 characters long")
            return false
        }

        return true
    }

    private fun isPasswordValid(password: String): Boolean {
        val isRequiredLength = password.length >= 8
        val hasLowercaseLetter = Pattern.compile("[a-z]").matcher(password).find()
        val hasUppercaseLetter = Pattern.compile("[A-Z]").matcher(password).find()
        val hasDigit = Pattern.compile("[0-9]").matcher(password).find()
        val hasSpecial =
            Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]").matcher(password).find()

        if (password.isBlank() || !isRequiredLength) {
            setDataValid(false)

            setErrorMessage(_passwordErrorMessage, "Password must be at least 8 characters long")
            return false
        }

        if (!hasLowercaseLetter || !hasUppercaseLetter) {
            setDataValid(false)

            setErrorMessage(
                _passwordErrorMessage,
                "Password must contain at least 1 capital and 1 lowercase letter"
            )

            return false
        }

        if (!hasDigit || !hasSpecial) {
            setDataValid(false)

            setErrorMessage(
                _passwordErrorMessage,
                "Password must contain at last 1 number and 1 special character"
            )

            return false
        }

        return true
    }

    private fun isEmailValid(email: String): Boolean {
        if (email.isBlank()) {
            setDataValid(false)

            setErrorMessage(_emailErrorMessage, "Email field is required")
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).find()) {
            setDataValid(false)

            setErrorMessage(_emailErrorMessage, "Please enter a valid email")
            return false
        }

        return true
    }
}

class UserViewModelFactory(private val repository: UserRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return UserViewModel(repository) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}